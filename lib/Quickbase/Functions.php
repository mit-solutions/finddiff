<?php

class Quickbase_Functions {
    
    
    public static function getDate( $date, $format = '' ) {
        $date = $date / 1000 ;
        $base = mktime(0, 0, 0, 1, 1, 1970) ;
        if ( $format )
            return date($date + $base, $format) ;
        else
            return $date + $base ;
    }
    
}