<?php

/* ----------------------------------------------------------------------
  Title : QuickBase PHP SDK
  Author : Joshua McGinnis (joshua_mcginnis@intuit.com)
  Description : The QuickBase PHP SDK is a simple class for interaction with the QuickBase REST API.
  The QuickBase API is well documented here:
  https://www.quickbase.com/up/6mztyxu8/g/rc7/en/va/QuickBaseAPI.htm

  License: Eclipse Public License
  http://www.eclipse.org/legal/epl-v10.html
  ----------------------------------------------------------------------- */

Class Quickbase_Connector {
    /* ---------------------------------------------------------------------
      // User Configurable Options
      ----------------------------------------------------------------------- */

    private $user_name;
    private $passwd;
    private $app_token;
    private $qb_site = "www.quickbase.com";
    private $qb_ssl = "https://www.quickbase.com/db/";
    private $ticketHours = 0;
    private $xml_mode = true;
    private $ticket;
    private $userid;
    private $errcode = 0;
    private $errtext = '';
    private $xml = true;
    private $db_id = '';  // Table/Database ID of the QuickBase being accessed

    /* ---------------------------------------------------------------------
      //	Do Not Change
      ----------------------------------------------------------------------- */
    var $input = "";
    var $output = "";

    /* -------------------------------------------------------------------- */

    /**
     * 
     * @param string $token
     * @param string $realm
     * @param bool $xml_mode
     */
    public function __construct($realm, $token = false, $xml_mode = true) {
        $this->app_token = $token;

        if ($realm) {
            $this->qb_site = $realm . '.quickbase.com';
            $this->qb_ssl = 'https://' . $realm . '.quickbase.com/db/';
        }

        $this->xml_mode = $xml_mode;
    }

    /**
     * 
     * @param string $user
     * @param string $pass
     * @param int $hours
     * @return boolean
     */
    public function Authenticate($user, $pass, $hours = 12) {
        if ($this->xml_mode) {
            $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
            $xml->addChild('username', $user);
            $xml->addChild('password', $pass);
            $xml->addChild('hours', (int) $hours);

            $response = $this->transmit($xml->asXML(), 'API_Authenticate', $this->qb_ssl . "main");
        } else {
            $params = array(
                'act' => 'API_Authenticate',
                'username' => $user,
                'password' => $pass,
                'hours' => (int) $hours
            );
            $url = $this->qb_ssl . '?' . http_build_query($params);
            $response = $this->transmit($url);
        }
        if ($response->errcode != 0) {
            $this->errcode = (string) $response->errcode;
            $this->errtext = (string) $response->errtext;
            return false;
        }

        $this->ticket = (string) $response->ticket;
        $this->userid = (string) $response->userid;

        return true;
    }

    public function getError() {
        return $this->errcode . ': ' . $this->errtext;
    }

    /**
     * 
     * @param bool $bool
     */
    public function setXmlMode($bool) {
        $this->xml_mode = $bool;
    }

    private function transmit($input, $action_name = "", $url = "") {
        
        $this->errcode = 0 ;
        $this->errtext = "" ;
        if ($this->xml_mode) {
            if ($url == "") {
                $url = $this->qb_ssl . $this->db_id;
            }
            $content_length = strlen($input);
            $headers = array(
                "POST /db/" . $this->db_id . " HTTP/1.0",
                "Content-Type: text/xml;",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                "Pragma: no-cache",
                "Content-Length: " . $content_length,
                'QUICKBASE-ACTION: ' . $action_name
            );
            $this->input = $input;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
            curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        } else {
            $ch = curl_init($input);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            $this->input = $input;
            curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        }
        $r = curl_exec($ch);

        $response = new SimpleXMLElement($r);

        return $response;
    }

    /**
     * 
     * @param string $dbid
     * @param array $fields
     * @param array $uploads
     * @return boolean | array( 'rid', 'update_id' )
     */
    public function addRecord($dbid, $fields, $uploads = array()) {
        if ($this->xml_mode) {

            $this->db_id = $dbid;

            $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
            $i = 0;
            foreach ($fields as $field) {
                $safe_value = preg_replace('/&(?!\w+;)/', '&', $field['value']);
                $xml->addChild('field', $safe_value);
                $xml->field[$i]->addAttribute('fid', $field['fid']);
                $i++;
            }
            foreach ($uploads as $upload) {
                $xml->addChild('field', $upload['value']);
                $xml->field[$i]->addAttribute('fid', $upload['fid']);
                $xml->field[$i]->addAttribute('filename', $upload['filename']);
                $i++;
            }

            if ($this->app_token) {
                $xml->addChild('apptoken', $this->app_token);
            }

            $xml->addChild('ticket', $this->ticket);
            $response = $this->transmit($xml->asXML(), 'API_AddRecord');
        } else {

            $params = array(
                'act' => 'API_AddRecord',
                'ticket' => $this->ticket,
                '_fid_8' => 'value'
            );

            foreach ($fields as $field) {
                $params['_fid_' . $field['fid']] = $field['value'];
            }

            if ($this->app_token) {
                $params['apptoken'] = $this->app_token;
            }

            $url = $this->qb_ssl . $dbid . '?' . http_build_query($params);
            $response = $this->transmit($url);
        }

        if ($response->errcode != 0) {
            $this->errcode = (string) $response->errcode;
            $this->errtext = (string) $response->errtext;
            return false;
        }

        return array(
            'rid' => (string) $response->rid,
            'update_id' => (string) $response->update_id
        );
    }

    /**
     * 
     * @param type $queries
     * @param type $qid
     * @param type $qname
     * @param type $clist
     * @param type $slist
     * @param type $fmt
     * @param type $options
     * @return array | boolean
     */
    public function doQuery($dbid, $queries = 0, $qid = 0, $qname = 0, $clist = 0, $slist = 0, $fmt = 'structured', $options = "") {

        if ($this->xml_mode) {

            $this->db_id = $dbid;

            $xml = new SimpleXMLElement('<qdbapi></qdbapi>');

            if ($queries) {
                $qString = "";
                foreach ($queries as $query) {
                    $criteria = "";
                    if ($qString != "") {
                        $criteria .= $query['ao'];
                    }
                    $criteria .= "{'" . $query['fid'] . "'."
                            . $query['ev'] . ".'"
                            . $query['cri'] . "'}";

                    $qString .= $criteria;
                }
                if ($qString != "") {
                    $xml->addChild('query', $qString);
                }
            } else if ($qid) {
                $xml->addChild('qid', $qid);
            } else if ($qname) {
                $xml->addChild('qname', $qname);
            }

            $xml->addChild('ticket', $this->ticket);
            $xml->addChild('fmt', $fmt);
            $xml->addChild('includeRids', 1);

            if ($this->app_token) {
                $xml->addChild('apptoken', $this->app_token);
            }
            if ($clist) {
                $xml->addChild('clist', $clist);
            }
            if ($slist) {
                $xml->addChild('slist', $slist);
                $xml->addChild('options', $options);
            }

            $response = $this->transmit($xml->asXML(), 'API_DoQuery');
        } else {

            $params = array(
                'act' => 'API_DoQuery',
                'ticket' => $this->ticket,
                'fmt' => $fmt,
                'includeRids' => 1
            );

            if ($queries) {
                $qString = "";
                foreach ($queries as $query) {
                    $criteria = "";
                    if ($qString != "") {
                        $criteria .= $query['ao'];
                    }
                    $criteria .= "{'" . $query['fid'] . "'."
                            . $query['ev'] . ".'"
                            . $query['cri'] . "'}";

                    $qString .= $criteria;
                }
                if ($qString != "") {
                    $params['query'] = $qString;
                }
            } else if ($qid) {
                $params['qid'] = $qid;
                $xml->addChild('qid', $qid);
            } else if ($qname) {
                $params['qname'] = $qname;
            }

            if ($clist)
                $params['clist'] = $clist;
            if ($slist)
                $params['slist'] = $slist;
            if ($options)
                $params['options'] = $options;

            $url = $this->qb_ssl . $dbid . "?" . http_build_query($params);

            $response = $this->transmit($url);
        }

        if ($response->errcode != 0) {
            $this->errcode = (string) $response->errcode;
            $this->errtext = (string) $response->errtext;
            return false;
        }

        $records = $response->xpath("/qdbapi/table/records/record");

        $data = array();
        foreach ($records as $R) {
            $fields = $R->xpath("f");
            $row = new Quickbase_Record();
            foreach ($fields as $F) {
                $fid = (string) $F->attributes()->id;
                $row[$fid] = (string) $F;
            }
            $row['record_id'] = (string) $R->attributes()->rid;
            $row['update_id'] = (string) $R->update_id;
            $data[] = $row;
        }

        return $data;
    }

    /* API_EditRecord: http://www.quickbase.com/api-guide/index.html#edit_record.html */

    public function edit_record($dbid, $rid, $fields, $uploads = 0) {

        if ($this->xml_mode) {

            $this->db_id = $dbid;

            $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
            $xml->addChild('rid', $rid);

            $i = 0;
            foreach ($fields as $field) {
                $safe_value = preg_replace('/&(?!\w+;)/', '&', $field['value']);
                $xml->addChild('field', $safe_value);
                $xml->field[$i]->addAttribute('fid', $field['fid']);
                $i++;
            }

            if ($uploads)
                foreach ($uploads as $upload) {
                    $xml->addChild('field', $upload['value']);
                    $xml->field[$i]->addAttribute('fid', $upload['fid']);
                    $xml->field[$i]->addAttribute('filename', $upload['filename']);
                    $i++;
                }

            if ($this->app_token) {
                $xml->addChild('apptoken', $this->app_token);
            }

            $xml->addChild('ticket', $this->ticket);
            $response = $this->transmit($xml->asXML(), 'API_EditRecord');
        } else {

            $params = array(
                'act' => 'API_EditRecord',
                'ticket' => $this->ticket,
                    //'_fid_8' => 'value'
            );

            foreach ($fields as $field) {
                $params['_fid_' . $field['fid']] = $field['value'];
            }

            if ($this->app_token) {
                $params['apptoken'] = $this->app_token;
            }

            $url = $this->qb_ssl . $dbid . '?' . http_build_query($params);
            $response = $this->transmit($url);
        }

        if ($response->errcode != 0) {
            $this->errcode = (string) $response->errcode;
            $this->errtext = (string) $response->errtext;
            return false;
        }

        return array(
            'update_id' => (string) $response->update_id
        );
    }

    /* API_ImportFromCSV: https://www.quickbase.com/up/6mztyxu8/g/rc7/en/va/QuickBaseAPI.htm#_Toc126580055 */

    public function import_from_csv($dbid, $records_csv, $clist, $skip_first = 0) {

        if ($this->xml_mode) {

            $this->db_id = $dbid;

            $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
            $xml->addChild('records_csv', $records_csv);
            $xml->addChild('clist', $clist);
            $xml->addChild('skipfirst', $skip_first);
            $xml->addChild('ticket', $this->ticket);

            if ($this->app_token) {
                $xml->addChild('apptoken', $this->app_token);
            }
            $response = $this->transmit($xml->asXML(), 'API_ImportFromCSV');
        } else {

            $params = array(
                'act' => 'API_ImportFromCSV',
                'records_csv' => $records_csv,
                'clist' => $clist,
                'skipfirst' => $skip_first,
                'ticket' => $this->ticket
            );

            if ($this->app_token) {
                $params['apptoken'] = $this->app_token;
            }

            $url = $this->qb_ssl . $dbid . "?" . http_build_query($params);
            $response = $this->transmit($url);
        }

        if ($response->errcode != 0) {
            $this->errcode = (string) $response->errcode;
            $this->errtext = (string) $response->errtext;
            return false;
        }

        return true;
    }

    /* API_RunImport */

    public function api_run_import($dbid, $id) {
        $this->db_id = $dbid ;
        if ($this->xml_mode) {
            
            $xml_packet = new SimpleXMLElement('<qdbapi></qdbapi>');
            $xml_packet->addChild('id', $id);

            $xml_packet->addChild('ticket', $this->ticket);
            
            if ( $this->app_token )
                $xml_packet->addChild('apptoken', $this->app_token);
            
            $xml_packet = $xml_packet->asXML();
            $response = $this->transmit($xml_packet, 'API_RunImport');
        } else {
            $url_string = $this->qb_ssl . $this->db_id . '?act=API_RunImport&ticket=' . $this->ticket . '&id=' . $id;
            $response = $this->transmit($url_string);
        }

        if ($response->errcode != 0) {
            $this->errcode = (string) $response->errcode;
            $this->errtext = (string) $response->errtext;
            return false;
        }

        return $response;
    }

}
