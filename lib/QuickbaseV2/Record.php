<?php

class QuickbaseV2_Record implements ArrayAccess {

    private $data = array();

    /**
     * 
     * @param array $data
     */
    public function __construct($data = array()) {
        $this->data = $data;
    }

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    public function getDate($fid, $format = '' ) {
        $date = null ;
        if (isset($this->data[$fid]) && $this->data[$fid]) {
            $qb_date = $this->data[$fid] / 1000;
            $base = mktime(0, 0, 0, 1, 1, 1970);
            $date = ($format ? date($format, $qb_date + $base) : $qb_date + $base);
        }
        return $date ;
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }

}
