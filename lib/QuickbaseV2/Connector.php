<?php

Class QuickbaseV2_Connector {

    private $app_token;
    private $user_token;
    private $qb_site = "www.quickbase.com";
    private $qb_ssl = "https://www.quickbase.com/db/";
    private $ticket;
    private $userid;
    private $errcode = 0;
    private $errtext = '';
    private $errdetail = '';

    /**
     * 
     * @param string $token
     * @param string $realm
     * @param bool $xml_mode
     */
    public function __construct($realm, $token = false) {
        $this->app_token = $token;

        if ($realm) {
            $this->qb_site = $realm . '.quickbase.com';
            $this->qb_ssl = 'https://' . $realm . '.quickbase.com/db/';
        }
    }

    public function getTicket() {
        return $this->ticket;
    }

    public function setTicket($value) {
        $this->ticket = $value;
    }

    /**
     * 
     * @param string $user
     * @param string $pass
     * @param int $hours
     * @return boolean
     */
    public function Authenticate($user, $pass, $hours = 12) {

        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
        $xml->addChild('username', $user);
        $xml->addChild('password', $pass);
        $xml->addChild('hours', (int) $hours);

        $response = $this->transmit($xml->asXML(), 'API_Authenticate', $this->qb_ssl . "main");

        if ($this->hasError())
            return false;

        $this->ticket = (string) $response->ticket;
        $this->userid = (string) $response->userid;
        return true;
    }

    public function setUserToken($userToken) {
        $this->user_token = $userToken;
    }

    public function hasError() {
        return $this->errcode > 0;
    }

    public function getError() {
        if ($this->errcode == 0)
            return false;

        $err = $this->errcode . ': ' . $this->errtext;
        if ($this->errdetail != '')
            $err .= '. ' . $this->errdetail;
        return $err;
    }

    private function transmit($input, $actionName = "", $url = "", $dbid = 'main') {

        $this->errcode = false;
        $this->errtext = '';
        $this->errdetail = '';

        if ($url == "") {
            $url = $this->qb_ssl . $dbid;
        }
        $contentLength = strlen($input);
        $headers = array(
            "POST /db/" . $dbid . " HTTP/1.0",
            "Content-Type: text/xml;",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-Length: " . $contentLength,
            'QUICKBASE-ACTION: ' . $actionName
        );
        $this->input = $input;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);

        $r = curl_exec($ch);

        $response = new SimpleXMLElement($r);

        if ($response && $response->errcode != 0) {
            $this->errcode = intval($response->errcode);
            $this->errtext = (string) $response->errtext;
            if ($response->errdetail) {
                $this->errdetail = (string) $response->errdetail;
            }
        }

        return $response;
    }

    public function getUserInfo($email) {

        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
        $xml->addChild('email', $email);
        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else
            $xml->addChild('ticket', $this->ticket);

        $response = $this->transmit($xml->asXML(), 'API_GetUserInfo', $this->qb_ssl . "main");

        if ($this->hasError())
            return false;

        return array(
            'id' => (string) $response->user->attributes()->id,
            'first_name' => (string) $response->user->firstName,
            'last_name' => (string) $response->user->lastName,
            'email' => (string) $response->user->email,
            'screen_name' => (string) $response->user->screenName,
            'is_verified' => (string) $response->user->isVerified,
            'external_auth' => (string) $response->user->externalAuth
        );
    }

    /**
     * 
     * @param type $queries
     * @param type $qid
     * @param type $qname
     * @param type $clist
     * @param type $slist
     * @param type $fmt
     * @param type $options
     * @return array | boolean
     */
    public function doQuery($dbid, $queries = 0, $qid = 0, $qname = 0, $clist = 0, $slist = 0, $fmt = 'structured', $options = "") {
        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');

        if ($queries) {
            $qString = "";
            foreach ($queries as $query) {
                $criteria = "";
                if ($qString != "") {
                    $criteria .= $query['ao'];
                }
                $criteria .= "{'" . $query['fid'] . "'."
                        . $query['ev'] . ".'"
                        . $query['cri'] . "'}";

                $qString .= $criteria;
            }
            if ($qString != "") {
                $xml->addChild('query', $qString);
            }
        } else if ($qid) {
            $xml->addChild('qid', $qid);
        } else if ($qname) {
            $xml->addChild('qname', $qname);
        }

        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        $xml->addChild('fmt', $fmt);
        $xml->addChild('includeRids', 1);

        if ($this->app_token) {
            $xml->addChild('apptoken', $this->app_token);
        }
        if ($clist) {
            $xml->addChild('clist', $clist);
        }
        if ($slist) {
            $xml->addChild('slist', $slist);
            $xml->addChild('options', $options);
        }

        $response = $this->transmit($xml->asXML(), 'API_DoQuery', '', $dbid);

        if ($this->hasError()) {
            return false;
        }

        $records = $response->xpath("/qdbapi/table/records/record");

        $data = array();
        foreach ($records as $R) {
            $fields = $R->xpath("f");
            $row = new QuickbaseV2_Record();
            foreach ($fields as $F) {
                $fid = (string) $F->attributes()->id;
                $row[$fid] = (string) $F;
            }
            $row['record_id'] = (string) $R->attributes()->rid;
            $row['update_id'] = (string) $R->update_id;
            $data[] = $row;
        }

        return $data;
    }

    public function getFileContent($dbid, $fid, $rid) {

        $url = "https://" . $this->qb_site . "/up/{$dbid}/a/r{$rid}/e{$fid}/v0";

        if ($this->user_token) {
            $url .= "?usertoken=" . $this->user_token;
        } else if ($this->ticket) {
            $url .= "?ticket=" . $this->ticket;
        }

        $curlResponse = new QB_CurlResponse();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, array($curlResponse, 'setHeader'));

        $curlResponse->body = curl_exec($ch);
        $curlResponse->http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {
            $curlResponse->error = curl_error($ch);
        }
        curl_close($ch);

        if ($curlResponse->http_code >= 200 && $curlResponse->http_code < 300) {
            return $curlResponse->body;
        }

        return false;
    }

    /**
     * 
     * @param string $dbid
     * @param array $fields
     * @param array $uploads
     * @return boolean | array( 'rid', 'update_id' )
     */
    public function addRecord($dbid, $fields, $uploads = array()) {
        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
        $i = 0;
        foreach ($fields as $field) {
            $safe_value = preg_replace('/&(?!\w+;)/', '&', $field['value']);
            $xml->addChild('field', $safe_value);
            $xml->field[$i]->addAttribute('fid', $field['fid']);
            $i++;
        }
        foreach ($uploads as $upload) {
            $xml->addChild('field', $upload['value']);
            $xml->field[$i]->addAttribute('fid', $upload['fid']);
            $xml->field[$i]->addAttribute('filename', $upload['filename']);
            $i++;
        }

        if ($this->app_token) {
            $xml->addChild('apptoken', $this->app_token);
        }

        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        $response = $this->transmit($xml->asXML(), 'API_AddRecord', '', $dbid);

        if ($this->hasError())
            return false;

        return array(
            'rid' => (string) $response->rid,
            'update_id' => (string) $response->update_id
        );
    }

    /* API_EditRecord: http://www.quickbase.com/api-guide/index.html#edit_record.html */

    public function editRecord($dbid, $rid, $fields, $uploads = 0, $updateid = 0) {

        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
        $xml->addChild('rid', $rid);
        $i = intval(0);
        foreach ($fields as $field) {
            $safe_value = preg_replace('/&(?!\w+;)/', '&', $field['value']);
            $xml->addChild('field', $safe_value);
            $xml->field[$i]->addAttribute('fid', $field['fid']);
            $i++;
        }
        if ($uploads) {
            foreach ($uploads as $upload) {
                $xml->addChild('field', $upload['value']);
                $xml->field[$i]->addAttribute('fid', $upload['fid']);
                $xml->field[$i]->addAttribute('filename', $upload['filename']);
                $i++;
            }
        }
        if ($this->app_token)
            $xml->addChild('apptoken', $this->app_token);

        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        $xml = $xml->asXML();
        $response = $this->transmit($xml, 'API_EditRecord', '', $dbid);

        if ($this->hasError())
            return false;

        return $response;
    }

    public function deleteRecord($dbid, $rid) {

        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
        $xml->addChild('rid', $rid);

        if ($this->app_token)
            $xml->addChild('apptoken', $this->app_token);

        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        $xml = $xml->asXML();
        $response = $this->transmit($xml, 'API_DeleteRecord', '', $dbid);

        if ($this->hasError())
            return false;

        return $response;
    }

    /* API_ImportFromCSV: https://www.quickbase.com/up/6mztyxu8/g/rc7/en/va/QuickBaseAPI.htm#_Toc126580055 */

    public function importFromCsv($dbid, $records_csv, $clist, $skip_first = 0) {
        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
        $xml->addChild('records_csv', $records_csv);
        $xml->addChild('clist', $clist);
        $xml->addChild('skipfirst', $skip_first);

        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        if ($this->app_token) {
            $xml->addChild('apptoken', $this->app_token);
        }
        $response = $this->transmit($xml->asXML(), 'API_ImportFromCSV', '', $dbid);

        if ($this->hasError())
            return false;

        return $response;
    }

    /* API_RunImport */

    public function apiRunImport($dbid, $id) {
        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
        $xml->addChild('id', $id);

        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        if ($this->app_token)
            $xml->addChild('apptoken', $this->app_token);

        $xml = $xml->asXML();
        $response = $this->transmit($xml, 'API_RunImport', '', $dbid);

        if ($this->hasError())
            return false;

        return $response;
    }

    /**
     * API_UserRoles
     * @param type $appId
     */
    public function UserRoles($appId) {

        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');

        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        if ($this->app_token) {
            $xml->addChild('apptoken', $this->app_token);
        }
        $response = $this->transmit($xml->asXML(), 'API_UserRoles', '', $appId);

        if ($this->hasError())
            return false;

        $users = $response->xpath("/qdbapi/users/user");

        $data = array();
        foreach ($users as $R) {
            $id = "" . $R->attributes()->id;
            $name = "" . $R->name;
            $data[$id] = $name;
        }

        return $data;
    }

    public function PurgeRecords($dbid, $queries = 0, $qid = 0, $qname = 0) {
        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');

        if ($queries) {
            $qString = "";
            foreach ($queries as $query) {
                $criteria = "";
                if ($qString != "") {
                    $criteria .= $query['ao'];
                }
                $criteria .= "{'" . $query['fid'] . "'."
                        . $query['ev'] . ".'"
                        . $query['cri'] . "'}";

                $qString .= $criteria;
            }
            if ($qString != "") {
                $xml->addChild('query', $qString);
            }
        } else if ($qid) {
            $xml->addChild('qid', $qid);
        } else if ($qname) {
            $xml->addChild('qname', $qname);
        }

        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        if ($this->app_token) {
            $xml->addChild('apptoken', $this->app_token);
        }

        $response = $this->transmit($xml->asXML(), 'API_PurgeRecords', '', $dbid);

        if ($this->hasError())
            return -1;

        return (string) $response->num_records_deleted;
    }

    public function getSchema($dbid) {

        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');
        
        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        if ($this->app_token)
            $xml->addChild('apptoken', $this->app_token);

        $xml = $xml->asXML();
        $response = $this->transmit($xml, 'API_GetSchema', '', $dbid);

        if ($this->hasError())
            return false;

        if ($response) {
            return $response;
        }
        return false;
    }

    public function grantedDbs() {

        $xml = new SimpleXMLElement('<qdbapi></qdbapi>');

        if ($this->user_token)
            $xml->addChild('usertoken', $this->user_token);
        else if ($this->ticket)
            $xml->addChild('ticket', $this->ticket);

        if ($this->app_token)
            $xml->addChild('apptoken', $this->app_token);

        $xml = $xml->asXML();

        $response = $this->transmit($xml, 'API_GrantedDBs', '', 'main');

        if ($this->hasError())
            return false;

        if ($response) {
            $tablas = array();
            $dbs = $response->xpath("/qdbapi/databases/dbinfo");
            foreach ($dbs as $R) {
                $tablas[(string) $R->dbid] = (string) $R->dbname;
            }
            return $tablas;
        }
        return false;
    }

}

class QB_CurlResponse {

    public $headers;
    public $body;
    public $http_code;

    public function setHeader($curl, $header) {
        $len = strlen($header);
        $header = explode(':', $header, 2);

        if (count($header) < 2) // ignore invalid headers
            return $len;

        $name = strtolower(trim($header[0]));
        if (!isset($this->headers[$name]))
            $this->headers[$name] = array(trim($header[1]));
        else
            $this->headers[$name][] = trim($header[1]);

        return $len;
    }

}
