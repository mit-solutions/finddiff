<?php

include 'bootstrap.php';
include 'SmartService.php';

$id = @$_GET['id'];
//$id = 11 ;

if ( !$id )
    PrintResponse(false, "El id de Factura es requerido.") ;

$quickbase = new Quickbase_Connector($qb_config['realm'], $qb_config['token'], true);

if (!$quickbase->Authenticate($qb_config['user'], $qb_config['pass'])) {
    $msg = "Error al conectarse a Quickbase: " . $quickbase->getError() . '\n';
    PrintResponse(false, $msg) ;
}


$query = array(
    array('fid' => 3, 'ev' => 'EX', 'cri' => $id )
);

$data = $quickbase->doQuery(TABLE_FACTURA, $query, 0, 0, "3.143." . TABLE_FID_XML) ;

if ( count($data) == 0 ) {
    PrintResponse(false, "No existe la factura solicitda " . $id) ;
}

$xml = $data[0][ TABLE_FID_XML ] . "" ;

$xmlcodcl = $data[0][143] . "" ;

$response = SmartService::SendInvoice($xmlcodcl, $xml) ;

if ( in_array($response["Estado"], array( 'enviado' )) ) {
    PrintResponse(true, "", $response ) ;
} else if ( in_array($response["Estado"], array( 'aceptado' )) ) {
    PrintResponse(false, $response["Err"], $response ) ;
} else if ( in_array($response["Estado"], array( 'rechazado' )) ) {
    PrintResponse(false, $response["Err"], $response ) ;
} else {
    PrintResponse(false, "Error inesperado", $response ) ;
}
