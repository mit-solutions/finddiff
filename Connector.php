<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Connector
 *
 * @author Victor
 */
class MySql_Connector {

    /**
     *
     * @var mysqli
     */
    private $mysqli;
    private $host;
    private $user;
    private $pass;
    private $db;
    public $err;

    public function __construct($host, $user, $pass, $db) {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->db = $db;
    }

    public function Connect() {
        $result = false;
        try {
            $this->mysqli = new mysqli($this->host, $this->user, $this->pass, $this->db);
            if ($this->mysqli->connect_errno) {
                $err = $this->mysqli->connect_errno . ": " . $this->mysqli->connect_error;
            } else {
                $result = true;
            }
        } catch (Exception $ex) {
            $err = $ex->getMessage();
        }

        return $result;
    }

    public function Disconnect() {
        try {
            $this->mysqli->close();
        } catch (Exception $ex) {
            
        }
    }

    public function ExecuteQuery($query) {

        $result = false;
        $data = array();

        if (!($result = $this->mysqli->query($query))) {
            $this->err = mysqli_error($this->mysqli);
            return null;
        }

        return $this->mysqli->affected_rows;
    }

    public function fetchAll($query) {

        $result = false;
        $data = array();

        if (!($result = $this->mysqli->query($query))) {
            $this->err = "Sorry, the website is experiencing problems.";
            return null;
        }

        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }

        $result->free();
        return $data;
    }

    public function fetchRow($query) {

        $result = false;
        $data = array();

        if (!($result = $this->mysqli->query($query))) {
            $this->err = "Sorry, the website is experiencing problems.";
            return null;
        }

        while ($row = $result->fetch_assoc()) {
            $data = $row;
            break;
        }

        if ($result)
            $result->free();

        return $data;
    }

    public function escape($value) {
        return $this->mysqli->real_escape_string($value);
    }

    /**
     * 
     * @param type $table
     * @param type $where
     * @param type $bind
     * @return int Number of rows affected
     */
    public function updateRow($table, $where, $bind) {
        $query = "UPDATE $table SET ";

        $set = array();
        foreach ($bind as $columnName => $value) {
            if ($value === false)
                $set[] = " {$columnName} = 0 ";
            else if ($value === true)
                $set[] = " {$columnName} = 1 ";
            else {
                $set[] = " {$columnName} = '" . $this->mysqli->real_escape_string($value) . "' ";
            }
        }

        $query .= implode(", ", $set);
        $query .= " WHERE " . $where;

        return $this->ExecuteQuery($query);
    }

    /**
     * 
     * @param type $table
     * @param type $where
     * @param type $bind
     * @return int Number of rows affected
     */
    public function createRow($table, $bind) {
        $query = "INSERT INTO $table ( " . implode(",", array_keys($bind)) . " ) VALUES ";

        $values = array();
        foreach ($bind as $columnName => $val) {
            if ($val === false)
                $values[] = "0";
            else if ($val === true)
                $values[] = "1";
            else {
                $values[] = "'" . $this->mysqli->real_escape_string($val) . "'";
            }
        }

        $query .= " ( " . implode(", ", $values) . " ) ;";

//echo $query ;

        return $this->ExecuteQuery($query);
    }

}
