<?php

class SmartService {

    private static function Invoke($methodName, $codigo_cliente, $xml) {
        try {
            $soapXml = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
            $soapXml .= "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";
            $soapXml .= "<soap:Body>";
            $soapXml .= "<{$methodName} xmlns=\"http://tempuri.org/\">";
            $soapXml .= "<codigo_cliente>" . $codigo_cliente . "</codigo_cliente>";
            $soapXml .= "<xml>" . htmlentities($xml) . "</xml>";
            $soapXml .= "</{$methodName}>";
            $soapXml .= "</soap:Body>";
            $soapXml .= "</soap:Envelope>";
            

            $cheaders = array('Content-Type: text/xml');

            $url = "http://facturaelectronica.smartstrategyapps.com/Service.asmx";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $cheaders);
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $soapXml);
            $response = curl_exec($ch);

            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);


            $doc = new DOMDocument();
            $doc->loadXML($body);

            $clave = $doc->getElementsByTagName('Clave')->item(0);
            if ($clave)
                $clave = $clave->nodeValue;

            $consecutivo = $doc->getElementsByTagName('Consecutivo')->item(0);
            if ($consecutivo)
                $consecutivo = $consecutivo->nodeValue;

            $estado = $doc->getElementsByTagName('Estado')->item(0);
            if ($estado)
                $estado = $estado->nodeValue;

            $fecha_emision = $doc->getElementsByTagName('FechaEmision')->item(0);
            if ($fecha_emision)
                $fecha_emision = $fecha_emision->nodeValue;

            $err = $doc->getElementsByTagName('err')->item(0);
            if ($err)
                $err = $err->nodeValue;


            $result = array(
                'Estado' => $estado,
                'Clave' => $clave,
                'Consecutivo' => $consecutivo,
                'FechaEmision' => $fecha_emision,
                'Err' => $err
            );

            if (!$result["Estado"]) {
                $result["Estado"] = "Error";
            }
            
            global $id ;
            RegisterLog($methodName, $result["Estado"] == "enviado" , $xml, $body, $id) ;

            return $result;
        } catch (Exception $ex) {
            $result = array(
                'Estado' => "Error",
                'Clave' => "",
                'Consecutivo' => "",
                'FechaEmision' => "",
                'Err' => $ex->getMessage()
            );
            return  $result;
        }
    }
    
    public static function SendInvoice($codigo_cliente, $xml) {
        return SmartService::Invoke("crearFactura", $codigo_cliente, $xml) ;
    }

    public static function SendCreditMemo($codigo_cliente, $xml) {
        return SmartService::Invoke("crearNotaCredito", $codigo_cliente, $xml) ;
    }

}
