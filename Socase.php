<?php

require_once("lib/soap/nusoap.php");
class Socase {

    private $webservice ;
    private $soap_client;
    
    public function __construct( $url ) {
        $this->webservice = $url;
        $this->soap_client = new nusoap_client($this->webservice . '?WSDL', true);
    }
    
    private function Invoke( $method, $data, &$message, &$realResponse ) {
        $result = false ;
        $response = $this->soap_client->call($method, $data);

        return $response;
    }

    public function SendInvoice( $data, &$message, &$realResponse ) {
        $result = $this->Invoke("crearFactura", $data, $message, $realResponse) ;
        return $result ;
    }

}