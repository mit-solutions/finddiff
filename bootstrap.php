<?php

date_default_timezone_set('America/Costa_Rica');

define('BASE_DIR', dirname(__FILE__));

define('DS', DIRECTORY_SEPARATOR);

set_include_path(
        get_include_path() . PATH_SEPARATOR .
        BASE_DIR . DS . 'lib' . PATH_SEPARATOR
);

include './lib/QuickbaseV2/Connector.php';
include './lib/QuickbaseV2/Record.php';



$qb_config = array(
  'user' => 'templates@smartstrategycr.com',
  'pass' => 'templates.2014',
  'token' => 'dcuvus7du74gfmdphgph6ch2yp7z',
  'realm' => 'smartstrategy',
  'usertoken' => 'bz9ry3_u9m_cm9fsvzhgxjjnd7wacurbndj8qp'
      );

//define("CODIGO_CLIENTE", "b9bc494d96c96b905eb879b24bb3665bddb92c00");
define("CODIGO_CLIENTE", "69");
define("TABLE_FACTURA", "bqsdkwq6y");
define("TABLE_FID_XML", "15");

function PrintResponse($success, $msg, $additional = array()) {

    $json_response = array(
        'success' => $success,
        'message' => $msg
    );

    if (count($additional) > 0) {
        $json_response["data"] = $additional;
    }

    echo "QBCallback(";
    echo json_encode($json_response, 1);
    echo ");";
    exit;
}

function RegisterLog($action, $success, $request, $response, $rid) {
    global $quickbase;

    $bind = array(
        array('fid' => 7, 'value' => $action),
        array('fid' => 9, 'value' => $success == true ? '1' : '0'),
        array('fid' => 6, 'value' => $response),
        array('fid' => 8, 'value' => $request),
        array('fid' => 11, 'value' => $rid)
    );

    $quickbase->addRecord("bqsg5fyej", $bind);
}
