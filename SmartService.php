<?php

require_once("lib/soap/nusoap.php");

class SmartService {

    /**
     *
     * @var nusoap_client
     */
    private $soap_client;
    private $webservice = "https://facturaelectronica.smartstrategyapps.com/Service.asmx";

    public function __construct() {
        $this->soap_client = new nusoap_client($this->webservice . '?WSDL', true);
    }

    private function Invoke($methodName, $codigo_cliente, $xml) {
        try {

            $data = array(
                'codigo_cliente' => $codigo_cliente,
                'xml' => $xml
            );
            
            $response = $this->soap_client->call($methodName, $data);

            $clave = "" ;
            $consecutivo = "" ;
            $estado = "Error" ;
            $fecha_emision = "" ;
            
            $err = false ;

            
            
            if ( isset($response["err"]) ) {
                $err = $response["err"] . "" ;
            }
            
            if ($response && is_array($response) && isset( $response[$methodName . "Result"] )) {
                $resp = $response[$methodName . "Result"] ;
                $clave = @$resp["Clave"];
                $consecutivo = @$resp["Consecutivo"];
                $estado = @$resp["Estado"];
                $fecha_emision = @$resp["FechaEmision"];
            }
            
            $result = array(
                'Estado' => $estado,
                'Clave' => $clave,
                'Consecutivo' => $consecutivo,
                'FechaEmision' => $fecha_emision,
                'Err' => $err
            );
           
            $body = print_r($response, 1);
            
            global $id;
            RegisterLog($methodName, $result["Estado"] == "enviado", $xml, $body, $id);

            return $result;
        } catch (Exception $ex) {
            $result = array(
                'Estado' => "Error",
                'Clave' => "",
                'Consecutivo' => "",
                'FechaEmision' => "",
                'Err' => $ex->getMessage()
            );
            return $result;
        }
    }

    public function SendInvoice($codigo_cliente, $xml) {
        return $this->Invoke("crearFactura", $codigo_cliente, $xml);
    }

    public function SendCreditMemo($codigo_cliente, $xml) {
        return SmartService::Invoke("crearNotaCredito", $codigo_cliente, $xml);
    }

}

class CurlResponseWS {

    public $headers;
    public $body;
    public $http_code;
    public $error;

    public function setHeader($curl, $header) {
        $len = strlen($header);
        $header = explode(':', $header, 2);
        if (count($header) < 2) // ignore invalid headers
            return $len;
        $name = strtolower(trim($header[0]));
        if (!isset($this->headers[$name]))
            $this->headers[$name] = array(trim($header[1]));
        else
            $this->headers[$name][] = trim($header[1]);

        return $len;
    }

}
